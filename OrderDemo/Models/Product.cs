﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OrderDemo.Models
{
    public class Product
    {
        [Column("ProductId")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public int ProjectId { get; set; }

        [Column("ProductName")]
        [Required]
        [StringLength(50)]
        public string ProjectName { get; set; }

        [Column("ProductCategory")]
        [Required]
        [StringLength(50)]
        public string ProductCategory { get; set; }
    }
}
