﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OrderDemo.Models
{
    public class Order
    {
        [Column("OrderId")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public int OrderId { get; set; }

        [Column("OrderItem")]
        [Required]
        [StringLength(50)]
        public string OrderItem { get; set; }

        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }

    }
}
