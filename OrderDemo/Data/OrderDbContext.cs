﻿using Microsoft.EntityFrameworkCore;
using OrderDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderDemo.Data
{
    public class OrderDbContext : DbContext
    {
        public OrderDbContext(DbContextOptions<OrderDbContext>options)
            : base(options)
        {

        }
        public DbSet<Order> Order { get; set; }

        public DbSet<Product> Product { get; set; }
    }
}
