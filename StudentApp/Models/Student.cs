﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace StudentApp.Models
{
    public class Student
    {
        [Column("Sid")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public int Sid { get; set; }

        [Column("Sname")]
        [Required]
        [StringLength(50)]
        public string Sname { get; set; }

        [Column("Saddress")]
        [Required]
        [StringLength(80)]
        public string Saddress { get; set; }
    }
}
