﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectApplication.Models;
using ProjectApplication.Repository;

namespace ProjectApplication.Controllers
{
    [Route("api/[controller]")]
    public class DeveloperController : ControllerBase
    {
        private readonly IDeveloperRepository developerRepo;
        public DeveloperController(IDeveloperRepository developerRepository)
        {
            developerRepo = developerRepository;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Developer>>> GetAllDeveloper()
        {
            var dev = await developerRepo.GetDevelopers();
            return Ok(dev);
        }
        [HttpPost]
        public async Task<ActionResult<Developer>> Post([FromBody] Developer dev)
        {
            await developerRepo.AddDeveloper(dev);
            return Ok();
        }
    }
}
