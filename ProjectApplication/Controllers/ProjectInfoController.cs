﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectApplication.Models;
using ProjectApplication.Repository;

namespace ProjectApplication.Controllers
{
    [Route("api/[controller]")]
    public class ProjectInfoController : ControllerBase
    {
        private readonly IProjectInfoRepository projectRepo;
        public ProjectInfoController(IProjectInfoRepository projectInfoRepository)
        {
            projectRepo = projectInfoRepository;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectInfo>>> GetAllProjectInfo()
        {
            var info = await projectRepo.GetInfos();
            return Ok(info);
        }
        [HttpPost]
        public async Task<ActionResult<ProjectInfo>> Post([FromBody] ProjectInfo info)
        {
            await projectRepo.AddInfos(info);
            return Ok();
        }
    }
}
