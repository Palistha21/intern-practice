﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectApplication.Models
{
    public class Developer
    {
        public int DevId { get; set; }
        public string DevName { get; set; }
        public string DevAddress { get; set; }
    }
}
