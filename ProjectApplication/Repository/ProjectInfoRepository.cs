﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectApplication.Models;
using ProjectApplication.Data;

namespace ProjectApplication.Repository
{
    public class ProjectInfoRepository : IProjectInfoRepository
    {
        public readonly ProjectInfoDbContext contextdb;
        public ProjectInfoRepository (ProjectInfoDbContext context)
        {
            contextdb = context;
        }
        public async Task<IEnumerable<ProjectInfo>> GetInfos()
        {
            var info = await contextdb.ProjectInfo.ToListAsync();
            return info;
        }
        public async Task AddInfos(ProjectInfo info)
        {
            await contextdb.ProjectInfo.AddAsync(info);
            await contextdb.SaveChangesAsync();
        }

    }
}
