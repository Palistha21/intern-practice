﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectApplication.Models;
using ProjectApplication.Data;

namespace ProjectApplication.Repository
{
    public interface IDeveloperRepository
    {
        Task<IEnumerable<Developer>> GetDevelopers();
        Task AddDeveloper(Developer dev);
    }
}
