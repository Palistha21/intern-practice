﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectApplication.Models;
using ProjectApplication.Data;

namespace ProjectApplication.Repository
{
    public class DeveloperRepository : IDeveloperRepository
    {
        private readonly DeveloperDbContext contextdb;
        public DeveloperRepository(DeveloperDbContext context)
        {
            contextdb = context;
        }
        public async Task<IEnumerable<Developer>> GetDevelopers()
        {
            var dev = await contextdb.Developer.ToListAsync();
            return dev;
        }
        public async Task AddDeveloper(Developer dev)
        {
            await contextdb.Developer.AddAsync(dev);
            await contextdb.SaveChangesAsync();
        }
    }
}
