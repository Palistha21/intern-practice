﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using ProjectApplication.Models;
using ProjectApplication.Repository;

namespace ProjectApplication.Data
{
    public class ProjectInfoDbContext : DbContext
    {
        public ProjectInfoDbContext(DbContextOptions<ProjectInfoDbContext> options)
            : base(options)
        {
        }
        public DbSet<ProjectInfo> ProjectInfo { get; set; }
    }
}

